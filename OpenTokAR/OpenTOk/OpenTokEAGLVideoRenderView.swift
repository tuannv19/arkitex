//
//  OpenTokEAGLVideoRenderView.swift
//  dating-calling
//
//  Created by Tuan on 10/29/19.
//  Copyright © 2019 HybridTechnologies. All rights reserved.
//

import UIKit
import OpenTok
import GLKit

protocol OpenTokEAGLVideoRenderViewDelegate {
    func renderer(_ renderer: OpenTokEAGLVideoRenderView, didReceiveFrame videoFrame: OTVideoFrame)
}

class OpenTokEAGLVideoRenderView: UIView {
    
    var delegate: OpenTokEAGLVideoRenderViewDelegate?
    
    fileprivate var glContext: EAGLContext?
    fileprivate var renderer: EAGLVideoRenderer?
    
    fileprivate var glkView: GLKView?
    
    fileprivate var frameLock: NSLock?
    
    fileprivate var renderingEnabled: Bool = true
    fileprivate var clearRenderer = 0
    
    fileprivate var lastVideoFrame: OTVideoFrame?
    
    fileprivate var displayLinkProxy: DisplayLinkProxy?
    fileprivate var displayLink: CADisplayLink?
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        glContext = EAGLContext(api:.openGLES2)
        renderer = EAGLVideoRenderer(context: glContext!)
        glkView = GLKView(frame: CGRect.zero, context: glContext!)
        
        glkView?.drawableColorFormat = .RGBA8888
        glkView?.drawableDepthFormat = .formatNone
        glkView?.drawableStencilFormat = .formatNone
        glkView?.drawableMultisample = .multisampleNone
        glkView?.delegate = self;
        glkView?.layer.masksToBounds = true;
        
        addSubview(glkView!)
        
        frameLock = NSLock()
        
        NotificationCenter.default
            .addObserver(self, selector: #selector(OpenTokEAGLVideoRenderView.willResignActive),
                         name: UIApplication.willResignActiveNotification, object: nil)
        NotificationCenter.default
            .addObserver(self, selector: #selector(OpenTokEAGLVideoRenderView.didBecomeActive),
                         name: UIApplication.didBecomeActiveNotification, object: nil)
        
        
        displayLinkProxy = DisplayLinkProxy(glkView: glkView!, videoRender: self)
        displayLink = CADisplayLink(target: displayLinkProxy!, selector:#selector(DisplayLinkProxy.displayLinkDidFire(_:)))
        displayLink!.frameInterval = 2
        displayLink!.add(to: RunLoop.main, forMode: RunLoop.Mode.common)
        
        renderer!.setupGL()
        
        displayLink!.isPaused = false
    }
    required init?(coder: NSCoder) {
        fatalError("Not implement")
    }
    
    @objc func willResignActive() {
        displayLink!.isPaused = true
        glkView?.deleteDrawable()
        renderer!.teardownGL()
    }
    
    @objc func didBecomeActive() {
        renderer!.setupGL()
        displayLink!.isPaused = false
    }
    
    
    var needsRendererUpdate: Bool {
        get {
            return renderer?.lastFrameTime != lastVideoFrame?.timestamp.value
                || clearRenderer != 0
        }
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        glkView?.frame = bounds
    }
    
}
extension OpenTokEAGLVideoRenderView : GLKViewDelegate {
    func glkView(_ view: GLKView, drawIn rect: CGRect) {
        defer {
            frameLock?.unlock()
        }
        frameLock?.lock()
        guard let frame = lastVideoFrame
            else {
                return
        }
        
        renderer?.drawFrame(frame: frame, withViewport: view.frame)
        
        let yPlane: UnsafeMutablePointer<GLubyte>? = frame.planes?.pointer(at: 0)?.assumingMemoryBound(to: GLubyte.self)
        let uPlane: UnsafeMutablePointer<GLubyte>? = frame.planes?.pointer(at: 1)?.assumingMemoryBound(to: GLubyte.self)
        let vPlane: UnsafeMutablePointer<GLubyte>? = frame.planes?.pointer(at: 2)?.assumingMemoryBound(to: GLubyte.self)
//        let planeSize = calculatePlaneSize(forFrame: frame)
        yPlane?.deallocate()
        uPlane?.deallocate()
        vPlane?.deallocate()
        
    }
    
}

extension OpenTokEAGLVideoRenderView : OTVideoRender{
    
    
    fileprivate func calculatePlaneSize(forFrame frame: OTVideoFrame)
        -> (ySize: Int, uSize: Int, vSize: Int)
    {
        guard let frameFormat = frame.format
            else {
                return (0, 0 ,0)
        }
        let baseSize = Int(frameFormat.imageWidth * frameFormat.imageHeight) * MemoryLayout<GLubyte>.size
        return (baseSize, baseSize / 4, baseSize / 4)
    }
    
    
    
    func renderVideoFrame(_ frame: OTVideoFrame) {
        if let fLock = frameLock, let format = frame.format {
            fLock.lock()
            assert(format.pixelFormat == .I420)
            
            lastVideoFrame = OTVideoFrame(format: format)
            lastVideoFrame?.timestamp = frame.timestamp
            
            let planeSize = calculatePlaneSize(forFrame: frame)
            let yPlane = UnsafeMutablePointer<GLubyte>.allocate(capacity: planeSize.ySize)
            let uPlane = UnsafeMutablePointer<GLubyte>.allocate(capacity: planeSize.uSize)
            let vPlane = UnsafeMutablePointer<GLubyte>.allocate(capacity: planeSize.vSize)
            
            memcpy(yPlane, frame.planes?.pointer(at: 0), planeSize.ySize)
            memcpy(uPlane, frame.planes?.pointer(at: 1), planeSize.uSize)
            memcpy(vPlane, frame.planes?.pointer(at: 2), planeSize.vSize)
            
            lastVideoFrame?.planes?.addPointer(yPlane)
            lastVideoFrame?.planes?.addPointer(uPlane)
            lastVideoFrame?.planes?.addPointer(vPlane)
            
            fLock.unlock()
            
            if let delegate = delegate {
                delegate.renderer(self, didReceiveFrame: frame)
            }
        }
    }
}

class DisplayLinkProxy {
    var renderer: OpenTokEAGLVideoRenderView
    var view: GLKView
    
    init(glkView: GLKView, videoRender: OpenTokEAGLVideoRenderView) {
        renderer = videoRender
        view = glkView
    }
    
    @objc func displayLinkDidFire(_ displayLink: CADisplayLink) {
        if renderer.needsRendererUpdate {
            view.setNeedsDisplay()
        }
    }
}
