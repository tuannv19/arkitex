//
//  FilteredPublisher.swift
//  OpenTokAR
//
//  Created by Tuan on 11/1/19.
//  Copyright © 2019 ccnv. All rights reserved.
//


import Foundation
import OpenTok
import GPUImage

class FilteredPublisher:  OTPublisherKit, OTVideoCapture, GPUImageVideoCameraDelegate {
    
    let imageHeight = 240
    let imageWidth = 320
    
    var videoCaptureConsumer: OTVideoCaptureConsumer?
    var videoCamera: GPUImageVideoCamera?
    let sepiaImageFilter = GPUImageSepiaFilter()
    var videoFrame : OTVideoFrame!
    var view = GPUImageView()
    
    //    override init() {
    //        super.init()
    //    }
    
    override init!(delegate: OTPublisherKitDelegate!) {
        super.init(delegate: delegate)
    }
    override init?(delegate: OTPublisherKitDelegate?, settings: OTPublisherKitSettings) {
        super.init(delegate: delegate, settings: settings)
        
        
        self.view = GPUImageView.init(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        self.videoCapture = self
        
//        let format = OTVideoFormat()
//        format.pixelFormat = OTPixelFormat.ARGB
//        format.imageWidth = UInt32(640)
//        format.imageHeight = UInt32(480)
        self.videoFrame =  OTVideoFrame(format: OTVideoFormat.init(nv12WithWidth: UInt32(UIScreen.main.bounds.width), height: UInt32(UIScreen.main.bounds.height)))
    }
    
    //    override init!(delegate: OTPublisherKitDelegate!, name: String!, audioTrack: Bool, videoTrack: Bool) {
    //        super.init(delegate: delegate, name: name, audioTrack: audioTrack, videoTrack: videoTrack)
    //    }
    
    
    //    override init!(delegate: OTPublisherKitDelegate!, name: String!){
    //        super.init(delegate: delegate, name: name)
    //
    //        self.view = GPUImageView.init(frame: CGRect.init(x: 0, y: 0, width: 1, height: 1))
    //        self.videoCapture = self
    //
    //        let format = OTVideoFormat()
    //        format.pixelFormat = OTPixelFormat.NV12
    //        format.imageWidth = UInt32(imageWidth)
    //        format.imageHeight = UInt32(imageHeight)
    //        self.videoFrame = OTVideoFrame(format: format)
    //    }
    
    func willOutputSampleBuffer(_ sampleBuffer: CMSampleBuffer!) {
        let imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer)
        CVPixelBufferLockBaseAddress(imageBuffer!, CVPixelBufferLockFlags(rawValue: CVOptionFlags(0)))
        videoFrame.clearPlanes()
        
        for i  in 0...CVPixelBufferGetPlaneCount(imageBuffer!) {
            videoFrame.planes?.addPointer(CVPixelBufferGetBaseAddressOfPlane(imageBuffer!, i))
        }
        
        videoFrame.orientation = OTVideoOrientation.left
        
        videoCaptureConsumer?.consumeFrame(videoFrame)  //crashes
        //
        CVPixelBufferUnlockBaseAddress(imageBuffer!, CVPixelBufferLockFlags(rawValue: CVOptionFlags(0)))
        
    }
    
    
    func initCapture(){
        
        
        videoCamera = GPUImageVideoCamera(sessionPreset: AVCaptureSession.Preset.hd1280x720.rawValue, cameraPosition: AVCaptureDevice.Position.front)
        videoCamera?.outputImageOrientation = UIInterfaceOrientation.portrait
        videoCamera?.delegate = self
//        videoCamera?.frameRate = 30
        
        
        let sepia = GPUImageBeautifyFilter()
        videoCamera?.addTarget(sepia)
        sepia.addTarget(self.view)
        
//        videoCamera?.addTarget(self.view)
        videoCamera?.startCapture()
    }
    
    
    func releaseCapture(){
        videoCamera?.delegate = nil
        videoCamera = nil
    }
    
    
    func start() -> Int32{
        return 0
    }
    
    
    func stop() -> Int32{
        return 0
    }
    
    
    func isCaptureStarted() -> Bool{
        return true
    }
    
    
    func captureSettings(_ videoFormat: OTVideoFormat) -> Int32{
        videoFormat.pixelFormat = OTPixelFormat.NV12
        videoFormat.imageWidth = UInt32(imageWidth)
        videoFormat.imageHeight = UInt32(imageHeight)
        return 0;
    }
}
