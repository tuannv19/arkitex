//
//  MainPageViewController.swift
//  OpenTokAR
//
//  Created by Tuan on 10/31/19.
//  Copyright © 2019 ccnv. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift
import FirebaseDatabase
//import SwiftJWT


let textLastOnline = "textLastOnline"
let textSessionValue = "textSessionValue"
let textConnect = "connections"

class MainPageViewController: UIViewController {
    
    let tableView : UITableView = UITableView.init()
    var users : [UserInfo] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupFirebase()
    }
    
    func setupView()  {
        
        tableView.register(UINib.init(nibName: "UserinfoTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        tableView.delegate = self
        tableView.dataSource = self
        
        self.view.addSubview(tableView)
        tableView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
    }
    
    func setupFirebase()  {

        FirebaseReposity.refDatabase.child("users").observe(.value) { (snapshot) in
            let postDict = snapshot.value as? [String : AnyObject] ?? [:]
            let users =  postDict.map { (key, value) -> UserInfo in
                var user = UserInfo.init()
                user.name = key
                
                if let valueDict = value as? NSDictionary {
                    user.onlineState = (valueDict[textConnect] != nil)  ? "Online" : "Offline"
                    if let sesstionValue =  valueDict[textSessionValue] {
                        user.sesstionValue = sesstionValue as? String
                    }
                }
                return user
            }
            self.users = users.filter{$0.name != UserIdentify.userID}
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
}

extension MainPageViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : UserinfoTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! UserinfoTableViewCell
        
        let user = users[indexPath.row]
        
        cell.lbName.text = user.name
        cell.lbState.text = user.onlineState
        cell.lbStreamSesstionState.text = user.sesstionValue != nil ? "busy" : "free"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 74
    }
}


struct UserInfo {
    var name: String?
    var sesstionValue : String?
    var onlineState : String?
    
}

class UserIdentify {
    
    private static let key = "keyuser0001"
    static var userID : String  {
        set {
            UserDefaults.standard.set(newValue, forKey: key)
            UserDefaults.standard.synchronize()
        }
        
        get{
            if let value = UserDefaults.standard.string(forKey: key) {
                return value
            }
            
            let randomeKey = "\(UInt.init(NSDate().timeIntervalSince1970))"
            UserIdentify.userID = randomeKey
            return randomeKey
        }
    }
}
class FirebaseReposity {
    
    static let refDatabase = Database.database().reference()
    
    static func registerUser() {
        
        // since I can connect from multiple devices, we store each connection instance separately
        // any time that connectionsRef's value is null (i.e. has no children) I am offline
        let myConnectionsRef = refDatabase.child("users/\(UserIdentify.userID)/connections")
        
        // stores the timestamp of my last disconnect (the last time I was seen online)
        let lastOnlineRef = refDatabase.child("users/\(UserIdentify.userID)/lastOnline")
        
        let connectedRef = Database.database().reference(withPath: ".info/connected")
        
        connectedRef.observe(.value, with: { snapshot in
            // only handle connection established (or I've reconnected after a loss of connection)
            guard let connected = snapshot.value as? Bool, connected else { return }
            
            // add this device to my connections list
            let con = myConnectionsRef.childByAutoId()
            
            // when this device disconnects, remove it.
            con.onDisconnectRemoveValue()
            
            // The onDisconnect() call is before the call to set() itself. This is to avoid a race condition
            // where you set the user's presence to true and the client disconnects before the
            // onDisconnect() operation takes effect, leaving a ghost user.
            
            // this value could contain info about the device or a timestamp instead of just true
            con.setValue(true)
            
            // when I disconnect, update the last time I was seen online
            lastOnlineRef.onDisconnectSetValue(ServerValue.timestamp())
        })
    }
    
}

class AlamofireReposity {
    //46443012
    //10bf9f733d60cb68bd156993ef903d51ff2fb586
}
