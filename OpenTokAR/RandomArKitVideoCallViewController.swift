//
//  RandomArKitVideoCallViewController.swift
//  dating-calling
//
//  Created by Tuan on 10/28/19.
//  Copyright © 2019 HybridTechnologies. All rights reserved.
//

import UIKit
import SnapKit
import ARKit
import OpenTok




class RandomArKitVideoCallViewController: UIViewController {
    
    var sceneView : ARSCNView!
    var incommingVideoView : UIView!
    
    
    var capturer: SCNViewVideoCapture?
    var otSession: OTSession?
    
    var otPublisher: OTPublisher?
    var otSubscriber: OTSubscriber?
    
    var otSessionDelegate: RandomArKitVideoCallViewControllerDelegate?
    var  configuration  : ARConfiguration?
    
    
    let noseOptions = ["👃", "🐽", "💧", " "]
    let eyeOptions = ["👁", "🌕", "🌟", "🔥", "⚽️", "🔎", " "]
    let mouthOptions = ["👄", "👅", "❤️", " "]
    let hatOptions = ["🎓", "🎩", "🧢", "⛑", "👒", " "]
    let features = ["nose", "leftEye", "rightEye", "mouth", "hat"]
    let featureIndices = [[9], [1064], [42], [24, 25], [20]]
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        
        let audio =  OTDefaultAudioDeviceWithVolumeControl.init()
        OTAudioDeviceManager .setAudioDevice(audio)
        
        print("Function: \(#function), line: \(#line)")
    }
    
    
    fileprivate func doSubscribe(_ stream: OTStream) {
        var error: OTError?
        defer {
            processError(error)
        }
        otSubscriber = OTSubscriber(stream: stream, delegate: self)
        
        otSession?.subscribe(otSubscriber!, error: &error)
        
    }
    
    func setupView(){
        
        incommingVideoView = UIView.init()
        incommingVideoView.backgroundColor = UIColor.systemPink

        self.view.addSubview(incommingVideoView)
        incommingVideoView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        
        
        sceneView = ARSCNView.init()
        sceneView.backgroundColor = .purple
        
        
        sceneView.delegate = self
        
        self.view.addSubview(sceneView)
        
        sceneView.snp.makeConstraints { (make) in
            make.width.equalTo(144.0 )
            make.height.equalTo(192.0)
            make.bottom.equalToSuperview().offset(10)
            make.left.equalToSuperview().offset(10)
        }
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupCapture()
        
        
        if  ARFaceTrackingConfiguration.isSupported  {
            configuration = ARFaceTrackingConfiguration()
        }else {
            configuration = ARWorldTrackingConfiguration()
        }
        
        sceneView.session.run(configuration!, options: [.resetTracking])
        sceneView.debugOptions = ARSCNDebugOptions.showWorldOrigin
        
        sceneView.showsStatistics = true
        
        print(sceneView.frame)
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        sceneView.session.pause()
    }
    
    
    override func viewDidLayoutSubviews() {
      super.viewDidLayoutSubviews()
        
//        print(sceneView.frame)
//        capturer?.width = Int.init(sceneView.subviews[0].bounds.width)//sceneView.frame.width
//        capturer?.height = Int.init(sceneView.subviews[0]bounds.height)//sceneView.frame.height
        
    }
    
    override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        if motion == .motionShake {
            deleteAllObjects()
        }
    }
    
    fileprivate func deleteAllObjects() {
        let nodes = sceneView.scene.rootNode.childNodes.filter { return $0.name == "ball" || $0.name == "star" || $0.name == "marker" }
        nodes.forEach({ node in
            node.removeFromParentNode()
        })
    }
    
    
    func setupCapture()  {
        
        capturer = SCNViewVideoCapture(sceneView: sceneView)
        
        capturer?.delegate = self
        
        otSessionDelegate = RandomArKitVideoCallViewControllerDelegate(self)
        otSession = OTSession(apiKey: kApiKey, sessionId: kSessionId, delegate: otSessionDelegate)
        
        let pubSettings = OTPublisherSettings()
        otPublisher = OTPublisher(delegate: self, settings: pubSettings)
        otPublisher?.videoCapture = capturer
        
        otSession?.connect(withToken: kToken, error: nil)
    }
    
    
    func updateFeatures(for node: SCNNode, using anchor: ARFaceAnchor) {
        for (feature, indices) in zip(features, featureIndices) {
            let child = node.childNode(withName: feature, recursively: false) as? EmojiNode
            let vertices = indices.map { anchor.geometry.vertices[$0] }
            child?.updatePosition(for: vertices)
            
            switch feature {
            case "leftEye":
                let scaleX = child?.scale.x ?? 1.0
                let eyeBlinkValue = anchor.blendShapes[.eyeBlinkLeft]?.floatValue ?? 0.0
                child?.scale = SCNVector3(scaleX, 1.0 - eyeBlinkValue, 1.0)
            case "rightEye":
                let scaleX = child?.scale.x ?? 1.0
                let eyeBlinkValue = anchor.blendShapes[.eyeBlinkRight]?.floatValue ?? 0.0
                child?.scale = SCNVector3(scaleX, 1.0 - eyeBlinkValue, 1.0)
            case "mouth":
                let jawOpenValue = anchor.blendShapes[.jawOpen]?.floatValue ?? 0.2
                child?.scale = SCNVector3(1.0, 0.8 + jawOpenValue, 1.0)
            default:
                break
            }
        }
    }
    
}

extension RandomArKitVideoCallViewController : SCNViewVideoCaptureDelegate {
    func prepare(videoFrame: OTVideoFrame) {
        let cameraNode = sceneView.scene.rootNode.childNodes.first {
            $0.camera != nil
        }
        if let node = cameraNode, let cam = node.camera {
            let data = Data(fromArray: [
                node.simdPosition.x,
                node.simdPosition.y,
                node.simdPosition.z,
                node.eulerAngles.x,
                node.eulerAngles.y,
                node.eulerAngles.z,
                Float(cam.zNear),
                Float(cam.fieldOfView)
            ])
            
            var err: OTError?
            videoFrame.setMetadata(data, error: &err)
            if let e = err {
                print("Error adding frame metadata: \(e.localizedDescription)")
            }
        }
    }
}


extension RandomArKitVideoCallViewController : ARSCNViewDelegate {
    
    func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
        guard let faceAnchor = anchor as? ARFaceAnchor,
            let device = sceneView.device else { return nil }
        let faceGeometry = ARSCNFaceGeometry(device: device)
        let node = SCNNode(geometry: faceGeometry)
        node.geometry?.firstMaterial?.fillMode = .lines

        node.geometry?.firstMaterial?.transparency = 0.0
        //            let noseNode = EmojiNode(with: noseOptions)
        //            noseNode.name = "nose"
        //            node.addChildNode(noseNode)
        //
        let leftEyeNode = EmojiNode(with: eyeOptions)
        leftEyeNode.name = "leftEye"
        leftEyeNode.rotation = SCNVector4(0, 1, 0, GLKMathDegreesToRadians(180.0))
        node.addChildNode(leftEyeNode)
        //
        let rightEyeNode = EmojiNode(with: eyeOptions)
        rightEyeNode.name = "rightEye"
        node.addChildNode(rightEyeNode)
        //
        //            let mouthNode = EmojiNode(with: mouthOptions)
        //            mouthNode.name = "mouth"
        //            node.addChildNode(mouthNode)

        let hatNode = EmojiNode(with: hatOptions)
        hatNode.name = "hat"
        node.addChildNode(hatNode)

        updateFeatures(for: node, using: faceAnchor)
        return node
    }
    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        guard let faceAnchor = anchor as? ARFaceAnchor, let faceGeometry = node.geometry as? ARSCNFaceGeometry else { return }

        faceGeometry.update(from: faceAnchor.geometry)
        updateFeatures(for: node, using: faceAnchor)
    }
    
    func sessionConnected() {
        otSession!.publish(otPublisher!, error: nil)
    }
}
//

extension RandomArKitVideoCallViewController : OTPublisherDelegate {
    func publisher(_ publisher: OTPublisherKit, didFailWithError error: OTError) {
        print("Function: \(#function), line: \(#line)")
    }
    
    func publisher(_ publisher: OTPublisherKit, streamCreated stream: OTStream) {
        print("Function: \(#function), line: \(#line)")
        
//        if otSubscriber == nil {
//            doSubscribe(stream)
//        }
        
    }
}



class RandomArKitVideoCallViewControllerDelegate : NSObject, OTSessionDelegate {
    let parent: RandomArKitVideoCallViewController
    
    init(_ parent: RandomArKitVideoCallViewController) {
        self.parent = parent
    }
    
    func session(_ session: OTSession, didFailWithError error: OTError) {
        print("Session Fail")
    }
    
    func session(_ session: OTSession, streamCreated stream: OTStream) {
        print("Stream Created")
        if parent.otSubscriber == nil {
            parent.doSubscribe(stream)
        }
    }
    
    func session(_ session: OTSession, streamDestroyed stream: OTStream) {
        print("Stream Destroyed")
    }
    
    func sessionDidConnect(_ session: OTSession) {
        print("SessionConnected")
        parent.sessionConnected()
    }
    
    func sessionDidDisconnect(_ session: OTSession) {
        print("Disconnect")
    }
    
    //    func session(_ session: OTSession, receivedSignalType type: String?, from connection: OTConnection?, with string: String?) {
    //        print("Received Signal\ntype:\(type ?? "nil") - \(string ?? "nil")")
    //        if type == "newNode", let coords = string{
    ////                        parent.addNewNode(withString: coords)
    //        } else if type == "deleteNodes" {
    ////                        parent.deleteAllObjects()
    //        }
    //    }
}

extension Data {
    init<T>(fromArray values: [T]) {
        var values = values
        self.init(buffer: UnsafeBufferPointer(start: &values, count: values.count))
    }
    func toArray<T>(type: T.Type) -> [T] {
        return self.withUnsafeBytes {
            [T](UnsafeBufferPointer(start: $0, count: self.count/MemoryLayout<T>.stride))
        }
    }
}


// MARK: - OTSubscriber delegate callbacks
extension RandomArKitVideoCallViewController: OTSubscriberDelegate {
    func subscriberDidConnect(toStream subscriberKit: OTSubscriberKit) {
        
        
        _ = self.incommingVideoView.subviews.map {$0.removeFromSuperview()}
        
        if let subsView = otSubscriber?.view {
            self.incommingVideoView.addSubview(subsView)
            subsView.snp.makeConstraints { (make) in
                make.edges.equalToSuperview()
            }
        }
    }
    func subscriber(_ subscriber: OTSubscriberKit, didFailWithError error: OTError) {
        print("Subscriber failed: \(error.localizedDescription)")
    }
    
    fileprivate func processError(_ error: OTError?) {
        if let err = error {
            DispatchQueue.main.async {
                let controller = UIAlertController(title: "Error", message: err.localizedDescription, preferredStyle: .alert)
                controller.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                self.present(controller, animated: true, completion: nil)
            }
        }
    }
}

