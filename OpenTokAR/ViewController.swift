//
//  ViewController.swift
//  Hello-World
//
//  Created by Roberto Perez Cubero on 11/08/16.
//  Copyright © 2016 tokbox. All rights reserved.
//

import UIKit
import OpenTok
import SnapKit

// *** Fill the following variables using your own Project info  ***
// ***            https://tokbox.com/account/#/                  ***
// Replace with your OpenTok API key
let kApiKey = "46443012"
// Replace with your generated session ID
let kSessionId = "2_MX40NjQ0MzAxMn5-MTU3MzAwODYyNTc3OX4yTXRCYmhBeXdrK2x4SXdKUVFvTDErMXR-UH4"
// Replace with your generated token
let kToken = "T1==cGFydG5lcl9pZD00NjQ0MzAxMiZzaWc9YmIwOTEzYWFiMzRkODI1NDFkNWE4ZGY5YzcwOTVlMjMzNDYxMjYyYTpzZXNzaW9uX2lkPTJfTVg0ME5qUTBNekF4TW41LU1UVTNNekF3T0RZeU5UYzNPWDR5VFhSQ1ltaEJlWGRySzJ4NFNYZEtVVkZ2VERFck1YUi1VSDQmY3JlYXRlX3RpbWU9MTU3MzAwODYyNSZyb2xlPW1vZGVyYXRvciZub25jZT0xNTczMDA4NjI1LjgwMjcyMzcyMzQxNzQ="


class ViewController: UIViewController {
    
    lazy var  vStackView  : UIStackView =  {
        let stackView = UIStackView.init()
        stackView.axis = .vertical
        stackView.distribution = .fillEqually
        return stackView    
    }()
    
    let subView = UIView()
    let pubView = UIView()
    
    
    lazy var session: OTSession = {
        return OTSession(apiKey: kApiKey, sessionId: kSessionId, delegate: self)!
    }()
    
    lazy var publisher: OTPublisher = {
        let settings = OTPublisherSettings()
        settings.name = UIDevice.current.name
        return OTPublisher(delegate: self, settings: settings)!
    }()
    
    var subscriber: OTSubscriber?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        print("Viewcontroller")
        
        self.view.addSubview(vStackView)
        vStackView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        vStackView.addArrangedSubview(subView)
        vStackView.addArrangedSubview(pubView)
        
        
        doConnect()
    }
    
    /**
     * Asynchronously begins the session connect process. Some time later, we will
     * expect a delegate method to call us back with the results of this action.
     */
    fileprivate func doConnect() {
        var error: OTError?
        defer {
            processError(error)
        }
        
        session.connect(withToken: kToken, error: &error)
    }
    
    /**
     * Sets up an instance of OTPublisher to use with this session. OTPubilsher
     * binds to the device camera and microphone, and will provide A/V streams
     * to the OpenTok session.
     */
    fileprivate func doPublish() {
        var error: OTError?
        defer {
            processError(error)
        }
        
        session.publish(publisher, error: &error)
        
        self.pubView.subviews.map {$0.removeFromSuperview()}
        
        if let pubView = publisher.view {
//            pubView.frame = CGRect(x: 0, y: 0, width: kWidgetWidth, height: kWidgetHeight)
//            view.addSubview(pubView)
            self.pubView.addSubview(pubView)
            pubView.snp.makeConstraints { (make) in
                make.edges.equalToSuperview()
            }
        }
    }
    
    /**
     * Instantiates a subscriber for the given stream and asynchronously begins the
     * process to begin receiving A/V content for this stream. Unlike doPublish,
     * this method does not add the subscriber to the view hierarchy. Instead, we
     * add the subscriber only after it has connected and begins receiving data.
     */
    fileprivate func doSubscribe(_ stream: OTStream) {
        var error: OTError?
        defer {
            processError(error)
        }
        subscriber = OTSubscriber(stream: stream, delegate: self)
        
        session.subscribe(subscriber!, error: &error)
    }
    
    fileprivate func cleanupSubscriber() {
        subscriber?.view?.removeFromSuperview()
        subscriber = nil
    }
    
    fileprivate func cleanupPublisher() {
        publisher.view?.removeFromSuperview()
    }
    
    fileprivate func processError(_ error: OTError?) {
        if let err = error {
            DispatchQueue.main.async {
                let controller = UIAlertController(title: "Error", message: err.localizedDescription, preferredStyle: .alert)
                controller.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                self.present(controller, animated: true, completion: nil)
            }
        }
    }
}

// MARK: - OTSession delegate callbacks
extension ViewController: OTSessionDelegate {
    func sessionDidConnect(_ session: OTSession) {
        print("Session connected")
        doPublish()
    }
    
    func sessionDidDisconnect(_ session: OTSession) {
        print("Session disconnected")
    }
    
    func session(_ session: OTSession, streamCreated stream: OTStream) {
        print("Session streamCreated: \(stream.streamId)")
        if subscriber == nil {
            doSubscribe(stream)
        }
    }
    
    func session(_ session: OTSession, streamDestroyed stream: OTStream) {
        print("Session streamDestroyed: \(stream.streamId)")
        if let subStream = subscriber?.stream, subStream.streamId == stream.streamId {
            cleanupSubscriber()
        }
    }
    
    func session(_ session: OTSession, didFailWithError error: OTError) {
        print("session Failed to connect: \(error.localizedDescription)")
    }
}

// MARK: - OTPublisher delegate callbacks
extension ViewController: OTPublisherDelegate {
    func publisher(_ publisher: OTPublisherKit, streamCreated stream: OTStream) {
        print("Publishing")
    }
    
    func publisher(_ publisher: OTPublisherKit, streamDestroyed stream: OTStream) {
        cleanupPublisher()
        if let subStream = subscriber?.stream, subStream.streamId == stream.streamId {
            cleanupSubscriber()
        }
    }
    
    func publisher(_ publisher: OTPublisherKit, didFailWithError error: OTError) {
        print("Publisher failed: \(error.localizedDescription)")
    }
}

// MARK: - OTSubscriber delegate callbacks
extension ViewController: OTSubscriberDelegate {
    func subscriberDidConnect(toStream subscriberKit: OTSubscriberKit) {
        
        
        self.subView.subviews.map {$0.removeFromSuperview()}
        
        if let subsView = subscriber?.view {
            self.subView.addSubview(subsView)
            subsView.snp.makeConstraints { (make) in
                make.edges.equalToSuperview()
            }
        }
    }
    func subscriber(_ subscriber: OTSubscriberKit, didFailWithError error: OTError) {
        print("Subscriber failed: \(error.localizedDescription)")
    }
}

