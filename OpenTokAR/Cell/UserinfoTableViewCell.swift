//
//  UserinfoTableViewCell.swift
//  OpenTokAR
//
//  Created by Tuan on 10/31/19.
//  Copyright © 2019 ccnv. All rights reserved.
//

import UIKit

class UserinfoTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var lbState: UILabel!
    @IBOutlet weak var lbStreamSesstionState: UILabel!
    @IBOutlet weak var btnButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
