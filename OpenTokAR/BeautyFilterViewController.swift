//
//  BeautyFilterViewController.swift
//  OpenTokAR
//
//  Created by Tuan on 11/1/19.
//  Copyright © 2019 ccnv. All rights reserved.
//

import UIKit

class BeautyFilterViewController: UIViewController {

     
        lazy var  vStackView  : UIStackView =  {
            let stackView = UIStackView.init()
            stackView.axis = .vertical
            stackView.distribution = .fillEqually
            return stackView
        }()
        
        let subView = UIView()
        let pubView = UIView()
        
        
        lazy var session: OTSession = {
            return OTSession(apiKey: kApiKey, sessionId: kSessionId, delegate: self)!
        }()
        
        lazy var publisher: FilteredPublisher = {
//            let settings = FilteredPublisher()
//            settings.name = UIDevice.current.name
//            return FilteredPublisher(delegate: self, settings: settings)!
            
            let publicser = FilteredPublisher.init(delegate: self)
//            publicser?.name = UIDevice.current.name
            
            return publicser!
            
        }()
        
        var subscriber: OTSubscriber?
        
        override func viewDidLoad() {
            super.viewDidLoad()
            
            
            print("Viewcontroller")
            
//            self.view.addSubview(vStackView)
//            vStackView.snp.makeConstraints { (make) in
//                make.edges.equalToSuperview()
//            }
//
//            vStackView.addArrangedSubview(subView)
//            vStackView.addArrangedSubview(pubView)
            
            self.view.addSubview(pubView)
            pubView.snp.makeConstraints { (make) in
                make.edges.equalToSuperview()
            }
            
            
            doConnect()
        }
        
        /**
         * Asynchronously begins the session connect process. Some time later, we will
         * expect a delegate method to call us back with the results of this action.
         */
        fileprivate func doConnect() {
            var error: OTError?
            defer {
                processError(error)
            }
            
            session.connect(withToken: kToken, error: &error)
        }
        
        /**
         * Sets up an instance of OTPublisher to use with this session. OTPubilsher
         * binds to the device camera and microphone, and will provide A/V streams
         * to the OpenTok session.
         */
        fileprivate func doPublish() {
            var error: OTError?
            defer {
                processError(error)
            }
            
            session.publish(publisher, error: &error)
            
            self.pubView.subviews.map {$0.removeFromSuperview()}
            
//            if let pubView = publisher.view {
    //            pubView.frame = CGRect(x: 0, y: 0, width: kWidgetWidth, height: kWidgetHeight)
    //            view.addSubview(pubView)
                self.pubView.addSubview(publisher.view)
                publisher.view.snp.makeConstraints { (make) in
                    make.edges.equalToSuperview()
                }
//            }
        }
        
        /**
         * Instantiates a subscriber for the given stream and asynchronously begins the
         * process to begin receiving A/V content for this stream. Unlike doPublish,
         * this method does not add the subscriber to the view hierarchy. Instead, we
         * add the subscriber only after it has connected and begins receiving data.
         */
        fileprivate func doSubscribe(_ stream: OTStream) {
            var error: OTError?
            defer {
                processError(error)
            }
            subscriber = OTSubscriber(stream: stream, delegate: self)
            
            session.subscribe(subscriber!, error: &error)
        }
        
        fileprivate func cleanupSubscriber() {
            subscriber?.view?.removeFromSuperview()
            subscriber = nil
        }
        
        fileprivate func cleanupPublisher() {
            publisher.view.removeFromSuperview()
        }
        
        fileprivate func processError(_ error: OTError?) {
            if let err = error {
                DispatchQueue.main.async {
                    let controller = UIAlertController(title: "Error", message: err.localizedDescription, preferredStyle: .alert)
                    controller.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                    self.present(controller, animated: true, completion: nil)
                }
            }
        }
    }

    // MARK: - OTSession delegate callbacks
    extension BeautyFilterViewController: OTSessionDelegate {
        func sessionDidConnect(_ session: OTSession) {
            print("Session connected")
            doPublish()
        }
        
        func sessionDidDisconnect(_ session: OTSession) {
            print("Session disconnected")
        }
        
        func session(_ session: OTSession, streamCreated stream: OTStream) {
            print("Session streamCreated: \(stream.streamId)")
            if subscriber == nil {
                doSubscribe(stream)
            }
        }
        
        func session(_ session: OTSession, streamDestroyed stream: OTStream) {
            print("Session streamDestroyed: \(stream.streamId)")
            if let subStream = subscriber?.stream, subStream.streamId == stream.streamId {
                cleanupSubscriber()
            }
        }
        
        func session(_ session: OTSession, didFailWithError error: OTError) {
            print("session Failed to connect: \(error.localizedDescription)")
        }
    }

    // MARK: - OTPublisher delegate callbacks
    extension BeautyFilterViewController: OTPublisherDelegate {
        func publisher(_ publisher: OTPublisherKit, streamCreated stream: OTStream) {
            print("Publishing")
        }
        
        func publisher(_ publisher: OTPublisherKit, streamDestroyed stream: OTStream) {
            cleanupPublisher()
            if let subStream = subscriber?.stream, subStream.streamId == stream.streamId {
                cleanupSubscriber()
            }
        }
        
        func publisher(_ publisher: OTPublisherKit, didFailWithError error: OTError) {
            print("Publisher failed: \(error.localizedDescription)")
        }
    }

    // MARK: - OTSubscriber delegate callbacks
    extension BeautyFilterViewController: OTSubscriberDelegate {
        func subscriberDidConnect(toStream subscriberKit: OTSubscriberKit) {
            
            
            self.subView.subviews.map {$0.removeFromSuperview()}
            
            if let subsView = subscriber?.view {
                self.subView.addSubview(subsView)
                subsView.snp.makeConstraints { (make) in
                    make.edges.equalToSuperview()
                }
            }
        }
        func subscriber(_ subscriber: OTSubscriberKit, didFailWithError error: OTError) {
            print("Subscriber failed: \(error.localizedDescription)")
        }
    }

